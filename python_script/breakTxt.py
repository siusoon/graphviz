# the small script is mainly for graphviz to parse text: remove punctuation and replace spaces with -> symbol
import fileinput
import string
import re
remove = string.punctuation
remove = remove.replace("-", "")
for line in fileinput.input(files ='input.txt'):
    print(line)
pattern = r"[{}]".format(re.escape(remove))
line = re.sub(pattern, "", line)
output=re.sub(r"\s", "->", line.rstrip())
print(output, file=open('output.txt', 'a'))
