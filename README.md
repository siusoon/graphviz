![](/Graphviz_code/tm_abstract.png)

# Python - data parsing

a simple python script for parsing data from an input file (usually a small abstract): 
1. remove all punctuation
2. replace space with "->"
3. export the final short text into an output.txt file for use 

The output file will be used in graphviz for diagram generation. 

## To run 

1. Fork/download the python_script folder
2. Replace your text in the input.txt 
3. Run the code in the terminal: `python3 breakTxt.py`
4. Check the output.txt
5. Insert the output text in [graphviz](https://graphviz.org/)

## To modify 

All punctuation will be removed except '-', you can add more rules by adding this line to replace '-' with something else that you want to exclude.

`remove = remove.replace("-", "")`

## Graphviz

1. Install [Graphviz](https://graphviz.gitlab.io/)
2. Fork/download the whole folder of "Graphviz_code"
3. To manually adjust what words come together: dot file 
4. Execute the script to generate diagrams in the terminal: 
- generate png file: `dot -Tpng tm_article.dot -o tm_article.png`
- geneerate svg file: `dot -Tsvg tm_article.dot -o tm_article.svg`

Credit: 
- font (Open Font Licence): https://www.librarystack.org/hershey-noailles-hershey-fonts/

## Licence 

COLLECTIVE CONDITIONS FOR RE-USE (CC4r)

See: 
- https://constantvzw.org/wefts/cc4r.en.html
- https://march.international/collectively-setting-conditions-for-re-use/
